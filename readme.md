# Interactive visualization of the covid dataset
The idea of this project, is to create a bokeh application that interactively plays with covid dataset.
# Setup
As interactive visualization canvas, `bokeh`is used... Run locally in your browser the application by executing:
```cmd:
bokeh serve --show app/map_projection.py
