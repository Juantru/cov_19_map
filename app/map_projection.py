import pandas as pd

from bokeh.io import curdoc
from bokeh.tile_providers import get_provider, Vendors
from bokeh.models import ColumnDataSource,Slider #ColorBar,Dropdown
from bokeh.plotting import figure
from bokeh.layouts import column, row

# TODO: Solve this issue, DLL load failed...
# from pyproj import Proj, transform


# from bokeh.palettes import Viridis256
# from bokeh.transform import linear_cmap

# Create the used methods
# def create_coordinates (long_arg, lat_arg):
#     in_wgs = Proj(init = "epsg:4326")
#     out_mercator = Proj(init = "epsg:3857")
#     long, lat = long_arg, lat_arg
#     mercator_x, mercator_y = transform(in_wgs, out_mercator, long, lat)
#     return mercator_x, mercator_y


# def create_circle(fig, x_data, y_data):
#     fig.circle(x=x_data, y=y_data)
#     return fig

# Load the data (1st some test data)
# Load us_dataset:
# us_data = pd.read_pickle("C:/Users/jtrul/Desktop/projects/covid_19/data/us_data.pkl")
# us_lon_lat_data = pd.read_pickle("C:/Users/jtrul/Desktop/projects/covid_19/data/state_lat_long_data.pkl")
# TODO: Open jupyter notebook, create the MErcator based coordinates running the create_coordinates method
# us_data.head()
# us_lon_lat_data.head()


list_x=[-8961219.008858522,-8000000.008858522 , -7000000.008858522]
list_y = [5281579.184590698, 6281579.184590698, 7281579.184590698]
x_slider_range = (min(list_x), max(list_y))
y_slider_range = (min(list_y), max(list_y))
data_points = ColumnDataSource({'x':list_x, 'y':list_y}) #, 'color_scale':pts[2]}) 
offset = Slider(title="Select data", value=0.0, start=0.0, end=len(list_x)-1, step=1)


# Set up plot
tile_provider = get_provider(Vendors.CARTODBPOSITRON)
fig = figure(title = 'Map', x_range = (-12000000, 9000000), y_range = (-1000000, 7000000))
fig.add_tile(tile_provider)
fig.scatter(x='x', y='y',source=data_points)

# Update data
def update_data(atrr, old, new):
    idx = offset.value
    x_idx=[list_x[idx]]
    y_idx=[list_y[idx]]
    data_points.data=dict(x=x_idx,
                         y=y_idx,
                         )

offset.on_change('value', update_data)

curdoc().add_root(column(fig, offset))
curdoc().title = "Sliders"